using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using tech_test_payment_api.Context;
using tech_test_payment_api.Models;

namespace tech_test_payment_api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class CadastroVendedorEVendaController : ControllerBase
    {
        private readonly AgendaContext _context;
        public CadastroVendedorEVendaController(AgendaContext context){
            _context = context;
        }

        [HttpPost("{Cadastrar Venda e Cadastrar Vendedor}")]
        public IActionResult Criar(CadastroVendedorEVenda cadastro){
            if(cadastro.Data == DateTime.MinValue)
            return BadRequest(new {Erro = "A data da venda não pode ser vazia"});
          
            _context.Add(cadastro);
            _context.SaveChanges();
            return Ok(cadastro);
        }

        [HttpGet("{Obter Por Status}")]
        public IActionResult ObterPorStatus(EnumStatusPagamento status){
            var pagamento = _context.Cadastros.Where(x => x.Status == status);
            return Ok(pagamento);
        }

        [HttpGet("{Buscar Venda Pelo Id}")]
        public IActionResult BuscarId(int id){
            var pagamento = _context.Cadastros.Find(id);
            if (pagamento == null)
            return NotFound();

            return Ok(id);
        }
        [HttpPut("{Atualizar Status de pagamento}")]
        public IActionResult Atualizar(EnumStatusPagamento status, CadastroVendedorEVenda cadastro){
            var pagamento= _context.Cadastros.Find(status);
            if(pagamento == null)
            return NotFound();

            pagamento.Status = cadastro.Status;
            
            _context.Update(pagamento);
            _context.SaveChanges();
            return Ok();

        }

        [HttpDelete("{id}")]
        public IActionResult Deletar(int id){
            var pagamento = _context.Cadastros.Find(id);
            if(pagamento == null)
            return NotFound();
             
             _context.Cadastros.Remove(pagamento);
            _context.SaveChanges();
            return NoContent();
        }
    }
}