namespace tech_test_payment_api.Models
{
    public enum EnumStatusPagamento
    {
        PagamentoAprovado,
        EnviadoATransportadora,
        Entregue,
        Cancelada
    }
}